package com.example.daohuyen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreCosmeticsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreCosmeticsServiceApplication.class, args);
    }
}
