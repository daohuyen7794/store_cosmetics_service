package com.example.daohuyen.common.customer.models.view;


import com.example.daohuyen.common.customer.models.data.Customer;

public class CustomerView {
    private String id;
    private String fullName;
    private String phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CustomerView() {
    }

    public CustomerView(Customer customer) {
        this.fullName = customer.getFullName();
        this.phone = customer.getPhone();
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
