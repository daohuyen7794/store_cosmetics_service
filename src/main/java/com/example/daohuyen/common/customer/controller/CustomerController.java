package com.example.daohuyen.common.customer.controller;

import com.example.daohuyen.auth.models.User;
import com.example.daohuyen.common.customer.dao.CustomerRespository;
import com.example.daohuyen.common.customer.models.body.CustomerBody;
import com.example.daohuyen.common.customer.models.data.Customer;
import com.example.daohuyen.response_model.NotFoundResponse;
import com.example.daohuyen.response_model.OkResponse;
import com.example.daohuyen.response_model.Response;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/customers")
@CrossOrigin(origins = "*")
public class CustomerController {
    @Autowired
    CustomerRespository customerRespository;
    /************* api đăng ký khách hàng ******************/
    @ApiOperation(value = "Đăng kí" , response = Iterable.class)
    @PostMapping("/register")
    Response registerCustomer(@RequestBody CustomerBody customerBody){
        Customer customer= new Customer();
        customer.setFullName(customerBody.getFullname());
        customer.setAddress(customerBody.getAddress());
        User user=new User(customerBody.getUsername(),customerBody.getPassword());
        customer.setUser(user);
        customerRespository.save(customer);
        return new OkResponse();
    }

    @ApiOperation(value = "Lay customer theo id" , response = Iterable.class)
    @GetMapping("/getcustomer/{id}")
    Response getCustomer(@PathVariable("id") String customerID){

        Customer customer= customerRespository.findByUser_Id(customerID);
        if(customer==null){
            return new NotFoundResponse("ko tiem thay id");
        }
        return new OkResponse(customer);
    }
    @ApiOperation(value = "update customer theo id" , response = Iterable.class)
    @PutMapping("/updatecustomer/{id}")
    Response updateCustomer(@PathVariable("id") String customerID,@RequestBody CustomerBody customerBody){

        Customer customer= customerRespository.findByUser_Id(customerID);
        if(customer==null){
            return new NotFoundResponse("ko tiem thay id");
        }
        customer.setFullName(customerBody.getFullname());
        customer.setAddress(customerBody.getAddress());
        customerRespository.save(customer);
        return new OkResponse(customer);
    }

}
